<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
    <style>
    .left {
      float: left;
      margin-right: 0;
      padding-right: 0px;
    }
    .right {
      float: right;
      width: 50%;
    }
    .clearfix{
      clear: both;
    }
    img {
      width: 90%;
    }
    p {
      font-size: 30px;
      font-weight: bold;
    }
    </style>
  </head>
  <body>

    <h2>My CD Collection</h2>

    <div class="left">
      <table border="1">
        <tr bgcolor="#9acd32">
          <th>Title</th>
          <th>Artist</th>
        </tr>
        <xsl:for-each select="catalog/cd">
        <xsl:sort select="artist" />
        <tr>
          <td><xsl:value-of select="title"/></td>
          <td><xsl:value-of select="artist"/></td>
        </tr>
        </xsl:for-each>
      </table>
    </div>
    
    <div class="right">
      <img src="bg.jpg"></img>
      <p>Hello, how are you?</p>  
    </div>

    <div class="clearfix"></div>

    <br></br><br></br>

    <a href="1tutorial-pg2.xml">Tutorial Page 2</a>

  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
